package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;


@org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
@RunWith(SpringRunner.class)
public class DataJpaTest {

    @Autowired
    private TestEntityManager tem;


    // 10. Why is JUnit not picking up this test?

    public void mapping(){

        LocalDate localDate = LocalDate.now();

        Task task = this.tem.persistFlushFind(new Task(null, "Mop the floors", localDate, false));

        Assertions.assertThat(task.getId()).isNotNull();

    }


}
