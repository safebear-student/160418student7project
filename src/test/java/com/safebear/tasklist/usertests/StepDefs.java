package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.ChromeDriver;
public class StepDefs {

    // 1. Why is this red?

    WebDriver driver;
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");
    final int SLEEP = Integer.parseInt(System.getProperty("sleep"));
    final String BROWSER = System.getProperty("browser");

    TaskListPage taskListPage;


    @Before
    public void setUp(){

        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;

        switch (BROWSER) {

            case "headless":

                //2. Why are these all red also?

                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless", "window-size=1920, 1080", "--no-sandbox");
                driver = new ChromeDriver(options);
                break;

            case "chrome":
                driver = new ChromeDriver();
                break;

            default:
                driver = new ChromeDriver();
                break;

        }


        taskListPage = new TaskListPage(driver);

        driver.get(url);

        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown(){

        try {
            Thread.sleep(SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }


    @When("^a user cooks a (.+)$")
    public void a_user_creates_a_task(String taskname) {

        taskListPage.addTask(taskname);

    }

    // 15. Why is my 'clean the windows' task not getting passed through to my code?

    @Then("^the (.+) appears in the tasklist$")
    public void the_task_appears_in_the_tasklist()  {

        Assertions.assertThat(taskListPage.checkForTask(String taskname)).isTrue();

    }

}
