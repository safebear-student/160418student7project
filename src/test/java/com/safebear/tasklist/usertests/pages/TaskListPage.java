package com.safebear.tasklist.usertests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TaskListPage {
    //WebDriver driver = new Chromedriver();
    //1. Why are these all red?

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "addTask")
    WebElement enterTaskField;


    public TaskListPage(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public void addTask(String taskname){

        enterTaskField.sendKeys(taskname);
        enterTaskField.sendKeys(Keys.ENTER);

    }

    public Boolean checkForTask(String taskname){

        wait.until(ExpectedConditions.visibilityOf(enterTaskField));
        return driver.findElements(By.xpath("//span[contains(text(),\"" + taskname + "\")]")).size() !=0;
    }

}
