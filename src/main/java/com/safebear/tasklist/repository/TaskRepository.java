package com.safebear.tasklist.repository;

import com.safebear.tasklist.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, Long>{


    List<Task> findByName(String taskname);

}
