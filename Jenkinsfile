pipeline {

    // Which jenkins server will be running the pipeline
    agent any

    parameters {

        // Test Names

        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API tests')
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')



        // Website parameters

        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://34.216.76.5', description: 'domain of the test environment')


        // Test Environment Parameter
        string(name: 'test_hostname', defaultValue: '34.216.76.5', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')

    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3' ))
    }

        // 5. poll at midnight every monday - why is this not working?

    triggers {
        pollSCM('* 1 * * 1')
    }

    stages {

        stage('Build with Unit Testing') {

            steps {

                sh 'mvn clean package'

            }

            post {

                success {

                    echo 'Now archiving...'

                    archiveArtifacts artifacts: '**/target/*.war'
                }

                always {

                    junit "**/target/surefire-reports/*.xml"
                }

            }

        }

        stage('Static Analysis') {

            steps {

                sh 'mvn checkstyle:checkstyle'

            }

            post {

                success {

                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''

                }

            }

        }



        stage('Deploy to Test') {

            steps {

//                3. This doesn't deploy! It's complaining that maven can't find a property called cargo.servlet.port.

                sh 'mvn -X cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password} -Dcargo.servlet.port=${params.test_port}'


            }

        }

        stage('Run API Tests') {

            steps {

                // 6. This was working on my windows machine, why's it not working on the jenkins linux box?

                bat 'mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}'
            }

            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }


        }


        stage('cucumber bdd tests') {

            steps {

                sh 'mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context} -Dsleep="0" -Dbrowser="headless"'

            }
            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber',
                            reportFiles          : 'index.html',
                            reportName           : 'BDD report',
                            reportTitles         : ''
                    ])
                }
            }

        }

    }


}